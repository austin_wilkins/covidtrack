class HistoricDataGetter {
    getData(stateToGet, callback){
        fetch("https://covidtracking.com/api/v1/states/daily.json")
        .then((data)=>{
            return data.json()
        }).then((data)=>{
          let stateResult = data.filter((item)=>{
            return item.state == stateToGet;
          }).reverse();
          console.log(stateResult);
          let stateDeaths = stateResult.map((item)=>{
            if (item.deathIncrease == undefined){
              return 0
            }
            return item.deathIncrease;
            });

        let stateHos = stateResult.map((item)=>{
          if (item.hospitalized == undefined){
            return 0
          }
          return item.hospitalized;
          });

      let statePos = stateResult.map((item)=>{
        if (item.positive == undefined){
          return 0
        }
        return item.positive;
        });
        callback(stateDeaths, stateHos, statePos);
    });
    }
}
module.exports = {HistoricDataGetter}
