import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {HistoricDataGetter} from './src/historicData.tsx';
import {Graph} from './component/graph.tsx';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 300,
  },
  spacer: {
    height: 300,
  }
});

var DataGetter = new HistoricDataGetter();

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {stateDeaths: [], stateHos: [], statePos: []}
  }

  componentDidMount(){
    DataGetter.getData("KS", (stateDeaths, stateHos, statePos)=>{
      this.setState({stateDeaths: stateDeaths, stateHos: stateHos, statePos: statePos})
    });
  }

  render(){
    if(this.state.stateDeaths.length == 0){
      return(
        <View style={styles.container}>
          <Text>Loading Data</Text>
        </View>
      )
    }
    return (
      <View style={styles.con}>
        <View style={styles.spacer}></View>
        <Graph data={this.state} />
      </View>
    );
  }
}
