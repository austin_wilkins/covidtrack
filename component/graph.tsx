import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { LineChart } from "react-native-chart-kit";

class Graph extends React.Component {
    render(){
      return (
        <View >
      <Text>Corona Time</Text>
      <LineChart
        data={{
          labels: ["Deaths", "Positive", "Hospitalized"],
          datasets: [
            {
              data:
                this.props.data.stateDeaths,
                color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
                strokeWidth: 2            },
            {
              data:
                this.props.data.stateHos,
                color: (opacity = 1) => `rgba(255, 0, 255, ${opacity})`,
                strokeWidth: 2
            },
            {
              data:
                this.props.data.statePos,
                color: (opacity = 1) => `rgba(50, 30, 150, ${opacity})`,
                strokeWidth: 2
            }
          ]
        }}
        width={Dimensions.get("window").width}
        height={200}
        yAxisLabel="$"
        yAxisSuffix="k"
        yAxisInterval={1}
        chartConfig={{
          backgroundColor: "#000000",
          backgroundGradientFrom: "#ffffff",
          backgroundGradientTo: "#000000",
          decimalPlaces: 2,
          color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          },
          propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "#000000"
          }
        }}
      />
    </View>
      );
    }
}


module.exports = {Graph}
